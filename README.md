# Celsius Probe for oscilloscope

This project is to use a temperature sensor as a probe in °K for displaying results on proportion °C per steps of 10milivolts using an oscilloscope as a screen to display the curve.

The license of the project is quite permissive, see [LICENSE](./LICENSE) file for more details, fork, use, modify it to fit your needs.

![icon](./pics/1164970.png)

See below for datasheets, schema, etc...

# Won't use a microcontroler

Easy & modern way to do so, might be to use a ADC included in a microcontroler or one of its analogical input & to code it in C, to display result on
serial port of a computer.

But there is no need of Intelligent _(microcontroler or logical circuit)_ to do that ; and it is a good way to learn more about using analog sensors with operational amplifiers.

# Conversion from Kelvin to Celsius

I choose the old method, to avoid the need of computer, or to add in project an external display, that would need to much energy to work with only a USB-DCDC converter to power it all.

Then there are fee difficulties in there

- [X] Choose DCDC converter that can manage usb 5volts input, & 15v symmetric output as main power *MEA1D0515SC*

- [X] choose a temperature sensor  *lm335*

- [X] choose an operational amplifier *OS77*, or even a little less efficient one might do the job

- [X] Build a comparator around lm335 & a voltage reference around that for _"mathematical"_ conversion from degrees kelvin to degrees Celsius.

It can be done by comparison of two values, from non-inverting input & inverting input of operational amplifier.

Here on inverting input, we read value from lm335 _(precision sensor)_ temperature sensor.

On non-inverting input, we use a component network to get a fixed -2.73v value.

Because °C = K - 273.15 _(as example : 297°K => becomes 23.85°C ; 2970 milivolts => 23.85°C)_.

# Then how to read it

On oscilloscope side, you choose your input-entry, as analogical _(if needed)_, select range 1v/div or 0.5v/div might be easier to read.

When temperature vary, you will see curve change until it stabilizes.

Try to have quite short wires from output to the oscilloscope _(maybe no more than 20 centimeters)_, avoid perturbations on signal.

If needed, the schema the diagram provides for the possibility of using or not a filter, with a simple switch.

# Datasheets

- LM335 [Temperature sensor LMx35](./datasheets/LMx35Temperature.pdf)

- MEA1D0515SC [5v to 15v symmetric DCDC](./datasheets/kdc_mea-45583.pdf)

- OP77 [Operational Amplifier OP77](./datasheets/OP77.pdf)

- SFM50 [TVS 5v SMF50](./datasheets/SMF50.pdf)

# Libs 

- project modified or added [kicad libraries](./kicad/local_lib/)

# Images

- [connectors view](./pics/ctpfo_Connectors.png)

- [copper side view](./pics/ctpfo_copper.png)

- [upper side view](./pics/ctpfo_UP.png)

# Pdf's

- [PCB_layers](./pdfs/PCB_layers.pdf)

- [black & white view of schema](./pdfs/schema.pdf)

- [colored schematic](./pdfs/schema_colors.pdf)
